package uz.gita.systemeventdetectorXP.data.local.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "event")
data class EventEntity(
    @PrimaryKey
    @ColumnInfo(name = "event_id")
    val eventId: Int,
    val image: Int,
    val name: Int,
    val indicator: Boolean,
    val action:String
)