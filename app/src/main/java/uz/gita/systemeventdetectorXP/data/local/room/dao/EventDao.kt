package uz.gita.systemeventdetectorXP.data.local.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import uz.gita.systemeventdetectorXP.data.local.room.entity.EventEntity

@Dao
interface EventDao {

    @Query("Select * from event")
    suspend fun getData(): List<EventEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addData(entity: EventEntity)

    @Query("delete from event where event_id =:id")
    suspend fun deleteData(id: Int)

}