package uz.gita.systemeventdetectorXP.data.local.room.db

import androidx.room.Database
import androidx.room.RoomDatabase
import uz.gita.systemeventdetectorXP.data.local.room.dao.EventDao
import uz.gita.systemeventdetectorXP.data.local.room.entity.EventEntity

@Database(entities = [EventEntity::class], version = 1, exportSchema = false)
abstract class AppDB : RoomDatabase() {
    abstract fun eventDao(): EventDao
}