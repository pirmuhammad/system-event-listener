package uz.gita.systemeventdetectorXP.data.model

data class EventModel(
    val id: Int,
    val text: Int,
    val image:Int,
    var indicator: Boolean,
    val action:String
)