package uz.gita.systemeventdetectorXP.domain.repository

import uz.gita.systemeventdetectorXP.data.local.room.entity.EventEntity
import uz.gita.systemeventdetectorXP.data.model.EventModel

interface AppRepository {

    suspend fun getDb(): List<EventModel>

    suspend fun deleteEvent(id: Int)

    suspend fun addEvent(eventModel: EventModel)

}