package uz.gita.systemeventdetectorXP.domain.repository.impl

import android.content.Intent
import uz.gita.systemeventdetectorXP.R
import uz.gita.systemeventdetectorXP.data.local.room.dao.EventDao
import uz.gita.systemeventdetectorXP.data.local.room.entity.EventEntity
import uz.gita.systemeventdetectorXP.data.model.EventModel
import uz.gita.systemeventdetectorXP.domain.repository.AppRepository
import javax.inject.Inject

class AppRepositoryImpl @Inject constructor(private val dao: EventDao) : AppRepository {
    override suspend fun getDb(): List<EventModel> {
        val list = dao.getData()
        var result: List<EventModel>
        if (list.isEmpty()) {
            result = getEvents().map {
                EventModel(it.eventId, it.name, it.image, it.indicator, it.action)
            }
        } else {
            result = getEvents().map {
                EventModel(it.eventId, it.name, it.image, it.indicator, it.action)
            }
            matchList(result, list)
        }
        return result
    }

    override suspend fun deleteEvent(id: Int) = dao.deleteData(id)

    override suspend fun addEvent(eventModel: EventModel) =
        dao.addData(
            EventEntity(
                eventId = eventModel.id,
                image = eventModel.image,
                name = eventModel.text,
                indicator = eventModel.indicator,
                action = eventModel.action
            )
        )


    private fun getEvents(): List<EventEntity> {
        return listOf(
            EventEntity(
                eventId = 0,
                image = R.drawable.ic_screen_on,
                name = R.string.screen_on,
                indicator = false,
                action = Intent.ACTION_SCREEN_ON
            ),

            EventEntity(
                eventId = 1,
                image = R.drawable.ic_screen_off,
                name = R.string.screen_off,
                indicator = false,
                action = Intent.ACTION_SCREEN_OFF
            ),

            EventEntity(
                eventId = 2,
                image = R.drawable.ic_connected,
                name = R.string.battery_charging_on,
                indicator = false,
                action = Intent.ACTION_POWER_CONNECTED
            ),

            EventEntity(
                eventId = 3,
                image = R.drawable.ic_disconnected,
                name = R.string.battery_charging_off,
                indicator = false,
                action = Intent.ACTION_POWER_DISCONNECTED
            ),

            EventEntity(
                eventId = 4,
                image = R.drawable.ic_storage_low,
                name = R.string.storage_low,
                indicator = false,
                action = Intent.ACTION_DEVICE_STORAGE_LOW
            ),
            EventEntity(
                eventId = 5,
                image = R.drawable.ic_airplane,
                name = R.string.text_airplane,
                indicator = false,
                action = Intent.ACTION_AIRPLANE_MODE_CHANGED
            ),
            EventEntity(
                eventId = 6,
                image = R.drawable.ic_battery_ok,
                name = R.string.text_battery_ok,
                indicator = false,
                action = Intent.ACTION_BATTERY_OKAY
            ),
            EventEntity(
                eventId = 7,
                image = R.drawable.ic_battery_low,
                name = R.string.text_battery_low,
                indicator = false,
                action = Intent.ACTION_BATTERY_LOW
            ),
            EventEntity(
                eventId = 8,
                image = R.drawable.ic_shutdown,
                name = R.string.text_shut_down,
                indicator = false,
                action = Intent.ACTION_SHUTDOWN
            ),
            EventEntity(
                eventId = 9,
                image = R.drawable.time_zone,
                name = R.string.text_time_zone_changed,
                indicator = false,
                action = Intent.ACTION_TIMEZONE_CHANGED
            ),
            EventEntity(
                eventId = 10,
                image = R.drawable.ic_time_changed,
                name = R.string.text_time_changed,
                indicator = false,
                action = Intent.ACTION_TIME_CHANGED
            ),
            EventEntity(
                eventId = 11,
                image = R.drawable.date_changed,
                name = R.string.text_date_changed,
                indicator = false,
                action = Intent.ACTION_DATE_CHANGED
            )
        )
    }

    private fun matchList(list: List<EventModel>, list2: List<EventEntity>) {
        list2.forEach { entity ->
            list.forEach {
                if (entity.eventId == it.id) it.indicator = true
            }
        }
    }


}