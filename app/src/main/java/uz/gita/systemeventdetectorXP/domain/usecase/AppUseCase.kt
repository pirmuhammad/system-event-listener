package uz.gita.systemeventdetectorXP.domain.usecase

import kotlinx.coroutines.flow.Flow
import uz.gita.systemeventdetectorXP.data.model.EventModel

interface AppUseCase {

    fun getEventList():Flow<List<EventModel>>

    suspend fun addEvent(eventModel: EventModel)

    suspend fun delete(id:Int)

}