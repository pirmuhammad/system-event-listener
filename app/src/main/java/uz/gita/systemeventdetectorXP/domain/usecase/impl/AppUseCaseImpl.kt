package uz.gita.systemeventdetectorXP.domain.usecase.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.gita.systemeventdetectorXP.data.model.EventModel
import uz.gita.systemeventdetectorXP.domain.repository.AppRepository
import uz.gita.systemeventdetectorXP.domain.usecase.AppUseCase
import javax.inject.Inject

class AppUseCaseImpl @Inject constructor(private val repository: AppRepository): AppUseCase {
    override fun getEventList(): Flow<List<EventModel>> = flow {
        emit(repository.getDb())
    }.flowOn(Dispatchers.IO)

    override suspend fun addEvent(eventModel: EventModel) = repository.addEvent(eventModel)

    override suspend fun delete(id: Int) = repository.deleteEvent(id)

}