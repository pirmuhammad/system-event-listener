package uz.gita.systemeventdetectorXP.presentation.screen

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.gita.systemeventdetectorXP.R
import uz.gita.systemeventdetectorXP.presentation.viewModel.SplashScreenViewModel
import uz.gita.systemeventdetectorXP.presentation.viewModel.impl.SplashScreenViewModelImpl

@AndroidEntryPoint
class SplashScreen:Fragment(R.layout.screen_splash) {

    private val viewModel:SplashScreenViewModel by viewModels<SplashScreenViewModelImpl>()
    private val openNextScreenObserver = Observer<Unit>{
        findNavController().navigate(SplashScreenDirections.actionSplashScreenToMainScreen())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.openNextScreenLiveData.observe(this@SplashScreen, openNextScreenObserver)
    }



}