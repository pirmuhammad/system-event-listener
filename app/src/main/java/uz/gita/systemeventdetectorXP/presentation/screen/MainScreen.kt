package uz.gita.systemeventdetectorXP.presentation.screen

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.gita.systemeventdetectorXP.R
import uz.gita.systemeventdetectorXP.data.model.EventModel
import uz.gita.systemeventdetectorXP.databinding.ScreenMainBinding
import uz.gita.systemeventdetectorXP.presentation.adapter.MainScreenAdapter
import uz.gita.systemeventdetectorXP.presentation.dialog.BottomSheetDialog
import uz.gita.systemeventdetectorXP.presentation.service.EventService
import uz.gita.systemeventdetectorXP.presentation.viewModel.MainScreenViewModel
import uz.gita.systemeventdetectorXP.presentation.viewModel.impl.MainScreenViewModelImpl

@AndroidEntryPoint
class MainScreen : Fragment(R.layout.screen_main) {

    private val binding by viewBinding<ScreenMainBinding>()
    private val viewModel: MainScreenViewModel by viewModels<MainScreenViewModelImpl>()
    private val adapter = MainScreenAdapter()
    private val bottomSheetDialog = BottomSheetDialog()
    private val eventListObserver = Observer<List<EventModel>>{
        adapter.submitList(it)
        val arrayList = ArrayList<String>()

        for (i in it.indices) {
            arrayList.add(it[i].action)
        }
        val intent = Intent(requireContext(), EventService::class.java)
        intent.putStringArrayListExtra("enabledActions", arrayList)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            requireActivity().startForegroundService(intent)
        } else {
            requireActivity().startService(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter.setOnItemClick { i, b, eventModel ->
            if (b) viewModel.addEvent(eventModel)
            else viewModel.deleteEvent(i)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        viewModel.getEventListLiveData.observe(viewLifecycleOwner, eventListObserver)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        moreButton.setOnClickListener {
            bottomSheetDialog.show(childFragmentManager, "")
        }
    }

}