package uz.gita.systemeventdetectorXP.presentation.viewModel

import androidx.lifecycle.LiveData
import uz.gita.systemeventdetectorXP.data.model.EventModel

interface MainScreenViewModel {

    val getEventListLiveData: LiveData<List<EventModel>>
    fun addEvent(eventModel: EventModel)
    fun deleteEvent(id:Int)

}