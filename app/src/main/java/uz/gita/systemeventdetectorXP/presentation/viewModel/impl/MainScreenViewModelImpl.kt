package uz.gita.systemeventdetectorXP.presentation.viewModel.impl

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.gita.systemeventdetectorXP.data.model.EventModel
import uz.gita.systemeventdetectorXP.domain.usecase.AppUseCase
import uz.gita.systemeventdetectorXP.presentation.viewModel.MainScreenViewModel
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModelImpl @Inject constructor(private val useCase: AppUseCase) :
    MainScreenViewModel, ViewModel() {

    init {
       getData()
    }

    override val getEventListLiveData = MutableLiveData<List<EventModel>>()

    override fun addEvent(eventModel: EventModel) {
        viewModelScope.launch(Dispatchers.IO) {
            useCase.addEvent(eventModel)
        }
    }

    override fun deleteEvent(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            useCase.delete(id)
        }
    }
    private fun getData(){
        viewModelScope.launch {
            useCase.getEventList().collectLatest {
                getEventListLiveData.postValue(it)
            }
        }
    }
}