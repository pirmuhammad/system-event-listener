package uz.gita.systemeventdetectorXP.presentation.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import uz.gita.systemeventdetectorXP.R
import uz.gita.systemeventdetectorXP.data.local.room.dao.EventDao
import javax.inject.Inject
@AndroidEntryPoint
class EventBroadcast : BroadcastReceiver() {
    private lateinit var mediaPlayer: MediaPlayer
    private val coroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

    @Inject
    lateinit var eventDao: EventDao

    override fun onReceive(context: Context?, intent: Intent?) {
        mediaPlayer = MediaPlayer.create(context, R.raw.sms)
        coroutineScope.launch {
            val events = eventDao.getData()
            for (i in events.indices) {
                when(intent?.action){
                    events[i].action -> {
                        mediaPlayer.start()
                    }
                }
            }
        }
    }

    fun cancelReceiver() {
        coroutineScope.cancel()
    }

}