package uz.gita.systemeventdetectorXP.presentation.viewModel.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import uz.gita.systemeventdetectorXP.presentation.adapter.MainScreenAdapter
import uz.gita.systemeventdetectorXP.presentation.viewModel.SplashScreenViewModel
import javax.inject.Inject

@HiltViewModel
class SplashScreenViewModelImpl @Inject constructor(): SplashScreenViewModel, ViewModel() {
    override val openNextScreenLiveData = MutableLiveData<Unit>()

    init {
        viewModelScope.launch {
            delay(4500)
            openNextScreenLiveData.value = Unit
        }
    }
}