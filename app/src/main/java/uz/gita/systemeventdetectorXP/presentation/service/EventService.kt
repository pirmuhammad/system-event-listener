package uz.gita.systemeventdetectorXP.presentation.service

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import uz.gita.systemeventdetectorXP.MainActivity
import uz.gita.systemeventdetectorXP.R
import uz.gita.systemeventdetectorXP.presentation.broadcast.EventBroadcast

class EventService : Service() {

    private val channelId = "Event"
    private val receiver = EventBroadcast()

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onCreate() {
        createChannel()
        startService()
    }

    override fun onDestroy() {
        super.onDestroy()
        receiver.cancelReceiver()
        unregisterReceiver(receiver)
    }

    private fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(channelId, "EVENT", NotificationManager.IMPORTANCE_DEFAULT)
            channel.setSound(null, null)
            val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            service.createNotificationChannel(channel)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return if (intent?.extras?.get("STOP") != "STOP") {
            val events = intent?.extras?.getStringArrayList("enabledActions")
            registerReceiver(receiver, IntentFilter().apply {
                for (i in events?.indices!!) {
                    addAction(events[i])
                }
            })
            START_NOT_STICKY
        } else {
            stopSelf()
            START_NOT_STICKY
        }
    }

    private fun createNotification(): Notification {
        val notifyIntent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val pendingIntent = PendingIntent
            .getActivity(
                this,
                0,
                notifyIntent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )

        return NotificationCompat
            .Builder(this, channelId)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.images)
            .setContentTitle("Event Sound")
            .setCustomContentView(getRemoterView())
            .build()
    }

    @SuppressLint("RemoteViewLayout")
    private fun getRemoterView(): RemoteViews {
        val view = RemoteViews(packageName, R.layout.notification)
        view.setOnClickPendingIntent(R.id.cancel, createPendingIntent())
        return view
    }

    private fun createPendingIntent(): PendingIntent {
        val intent = Intent(this, EventService::class.java)
        intent.putExtra("STOP", "STOP")
        return PendingIntent
            .getService(
                this,
                1,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
            )
    }
    private fun startService() {
        val notifyIntent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val pendingIntent = PendingIntent
            .getActivity(
                this,
                0,
                notifyIntent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )

        val notification = NotificationCompat
            .Builder(this, channelId)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.images)
            .setContentTitle("Event Sound")
            .setCustomContentView(getRemoterView())
            .build()

        startForeground(1, notification)
    }
}