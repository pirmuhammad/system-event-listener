package uz.gita.systemeventdetectorXP.presentation.viewModel

import androidx.lifecycle.LiveData

interface SplashScreenViewModel {
    val openNextScreenLiveData:LiveData<Unit>
}