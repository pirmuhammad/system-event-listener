package uz.gita.systemeventdetectorXP.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import uz.gita.systemeventdetectorXP.R
import uz.gita.systemeventdetectorXP.data.model.EventModel
import uz.gita.systemeventdetectorXP.databinding.ItemEventBinding

class MainScreenAdapter :
    ListAdapter<EventModel, MainScreenAdapter.ViewModel>(MainAdapterDiffUtil) {

    private var onItemIndicatorClick: ((Int, Boolean, EventModel) -> Unit)? = null


    object MainAdapterDiffUtil : DiffUtil.ItemCallback<EventModel>() {
        override fun areItemsTheSame(oldItem: EventModel, newItem: EventModel): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: EventModel, newItem: EventModel): Boolean =
            oldItem == newItem

    }

    inner class ViewModel(private val binding: ItemEventBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.indicator.setOnClickListener {
                val item = getItem(absoluteAdapterPosition)
                onItemIndicatorClick?.invoke(
                    item.id, binding.indicator.isChecked, item
                )
            }
        }

        fun bind(position: Int) = with(binding) {
            val item = getItem(position)
            eventImage.setImageResource(item.image)
            if (indicator.getTag(R.string.key) == null || indicator.getTag(R.string.key) != "get"){
                indicator.setTag(R.string.key, "get")
                indicator.isChecked = item.indicator
            }
            eventName.setText(item.text)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel = ViewModel(
        ItemEventBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        holder.bind(position)
    }

    fun setOnItemClick(bl: (Int, Boolean, EventModel) -> Unit) {
        onItemIndicatorClick = bl
    }

}