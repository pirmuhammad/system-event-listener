package uz.gita.systemeventdetectorXP.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import uz.gita.systemeventdetectorXP.domain.usecase.AppUseCase
import uz.gita.systemeventdetectorXP.domain.usecase.impl.AppUseCaseImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface UseCaseModule {

    @[Binds Singleton]
    fun bindUseCase(impl: AppUseCaseImpl):AppUseCase

}