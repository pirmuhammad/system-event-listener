package uz.gita.systemeventdetectorXP.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uz.gita.systemeventdetectorXP.data.local.room.dao.EventDao
import uz.gita.systemeventdetectorXP.data.local.room.db.AppDB
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @[Provides Singleton]
    fun getDatabase(@ApplicationContext context: Context): AppDB =
        Room
            .databaseBuilder(context, AppDB::class.java, "AppDb")
            .fallbackToDestructiveMigration()
            .build()

    @[Provides Singleton]
    fun getDao(appDB: AppDB):EventDao = appDB.eventDao()

}